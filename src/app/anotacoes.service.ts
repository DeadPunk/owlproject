import { Injectable } from '@angular/core';
import _ from 'lodash';
import { ANOTACAO } from './mock';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AnotacaoModel } from './model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnotacoesService {
 
  todo$: Promise<void>;
  todoCollectionRef: AngularFirestoreCollection<AnotacaoModel>;
 
  constructor(private db: AngularFirestore) {
    this.todoCollectionRef = this.db.collection('anotacoes');
    this.todo$ = this.todoCollectionRef.snapshotChanges().forEach(actions => {
      return actions.map(action => {
        const data = action.payload.doc.data() as AnotacaoModel;
        const id = action.payload.doc.id;
        return { id, ...data };
      });
    });
  }

  getAnotacoes(){
    return this.db.collection<AnotacaoModel>('anotacoes').valueChanges();
  }

  // createAnotacoes(anotacao: AnotacaoModel) {    
  //   return this.db.collection<AnotacaoModel>('anotacoes').add(anotacao);
  // }
  createAnotacoes(anotacao: AnotacaoModel) {    
    return this.db.collection<AnotacaoModel>('anotacoes').doc(anotacao.id).set(anotacao);
  }
  
  update(anotacao: AnotacaoModel) {
    this.todoCollectionRef.doc(anotacao.id).update({ nome: anotacao.nome, conteudo: anotacao.conteudo });
  }

  delete(anotacao: AnotacaoModel) {
    var teste = this.db.collection('anotacoes').doc(anotacao.id).delete();
  }

  getAnotacoesByName(nome){
    return _.filter(ANOTACAO, function(e){
      if(e.nome == nome){
        return e;
      }
    });
  }

  // criar(){
  //   var setSf = this.todoCollectionRef.doc('SF').set({
  //     nome: 'San Francisco', conteudo: 'Nova York'});
  // }

}
