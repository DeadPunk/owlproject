export class AudioModel {
    fileName: string;
    size: number;
    class: string;
    date: string;
}

export class AnotacaoModel {
    id?: string;
    nome: string;
    conteudo: string;
}