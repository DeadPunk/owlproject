import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnotacoesService } from '../anotacoes.service';
import { AudioService } from '../audio/audio.service';
import { AlertController } from '@ionic/angular';
import { AnotacaoModel } from '../model';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.page.html',
  styleUrls: ['./detalhes.page.scss'],
})
export class DetalhesPage implements OnInit {

  name: any;
  component: string;
  details: object;

  constructor(public route: ActivatedRoute, 
    private anotacoesService: AnotacoesService,
    private audioService: AudioService,
    private alertController: AlertController,
    private router: Router,
    ) { }

  ngOnInit() {

    this.route.queryParams.subscribe(
      (queryParams: any) => {
        this.name = queryParams['name'];
        this.component = queryParams['component'];
      }
    );
    this.getDetails();
  }

  // edit(){
  //   this.anotacoesService.editAnotacoes(anotacao).then().catch((err) => {
  //     console.log("Erro ao criar a anotação");
  //     console.log(err);
  //   });

  // }

  getDetails(){
    if (this.component == 'audio'){
      this.details = this.audioService.getAudioByName(this.name);
      console.log('Details: ', this.details);
      
      
    } else
    if (this.component == 'note'){
      this.details = this.anotacoesService.getAnotacoesByName(this.name);
    }
  }

  async excluir() {
    const alert = await this.alertController.create({
      header: 'Excluir anotação',
      message: 'Tem certeza que deseja excluir?',
      buttons: [
        {
          text: 'Não',
          cssClass: 'secondary'
        }, {
          text: 'Sim',
          handler: () => {
            console.log('Confirm Okay');
            this.delete();
          }
        }
      ]
    });

    await alert.present();
  }

  delete(){
    this.anotacoesService.delete({id: this.name, nome: '', conteudo:''});
    this.router.navigate(['/tabs/note']);

  }



}