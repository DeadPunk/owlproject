import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';
import { Routes } from '@angular/router';

const routes: Routes = [
	{
		path: 'tabs',
		component: TabsPage,
		children: [
		{	
			path: 'home',
			loadChildren: './tabs/home/tab1.module#HomeModule'

		},
		{	
			path: 'audio',
			loadChildren: './tabs/audio/audio.module#AudioModule'

		},
		{	
			path: 'camera',
			loadChildren: './tabs/camera/camera.module#CameraModule'

		},
		{	
			path: 'note',
			loadChildren: './tabs/note/note.module#NoteModule'

		},
		{
			path: '',
			redirectTo: '/tabs/home',
			pathMatch: 'full'

		}
		]
	}
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
