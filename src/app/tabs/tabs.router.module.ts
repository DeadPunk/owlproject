import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'camera',
        children: [
          {
            path: '',
            loadChildren: '../camera/camera.module#CameraModule'
          }
        ]
      },
      {
        path: 'audio',
        children: [
          {
            path: '',
            loadChildren: '../audio/audio.module#AudioModule'
          }
        ]
      },
      {
        path: 'note',
        children: [
          {
            path: '',
            loadChildren: '../note/note.module#NoteModule'
          }
        ]
      },
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: '../home/home.module#HomeModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TabsPageRoutingModule {}
