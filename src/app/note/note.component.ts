import { Component, OnInit} from '@angular/core';

import {AnotacoesService} from '../anotacoes.service'
import { ActivatedRoute, Router } from '@angular/router';
import { AnotacaoModel } from '../model';

@Component({
  selector: 'app-note', //tag html para depois usar o component
  templateUrl: './note.component.html', 
  styleUrls: ['./note.component.scss'],
})

export class NoteComponent implements OnInit {
    
  item: AnotacaoModel[];

  constructor(public anotacoesService: AnotacoesService,
    public route: ActivatedRoute) {
  }

  ngOnInit() {   
    this.getAnotacoes() ;
  }

  getAnotacoes() {
    this.anotacoesService.getAnotacoes().subscribe((anotacoes) => {
      this.item = anotacoes;
    }, (err) => {
      console.log("erro ao recuperar a lista")
      console.log(err);
    });
  }

  // criar(){
  //   this.anotacoesService.criar();
    
  // }

}
