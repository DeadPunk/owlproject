import { Component, OnInit} from '@angular/core';
import { AnotacoesService } from 'src/app/anotacoes.service';
import { AnotacaoModel } from 'src/app/model';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-note',
  templateUrl: './new-note.page.html',
  styleUrls: ['./new-note.page.scss'],
})

export class NewNotePage implements OnInit {
  
  constructor(public anotacoesService: AnotacoesService, public alertController: AlertController, private router: Router) {
  
  }

  nomeAnotacao: string;

  ngOnInit() {
  }

  salvarValor(valor, nomeAnotacao){
    const anotacao: AnotacaoModel = {
      id: nomeAnotacao,
      conteudo: valor,
      nome: nomeAnotacao
    }

    this.anotacoesService.createAnotacoes(anotacao).then().catch((err) => {
      console.log("Erro ao criar a anotação");
      console.log(err);
    });

    this.router.navigate(['/tabs/note']);
  }

  async salvar(valor) {
    const alert = await this.alertController.create({
      header: 'Deseja salvar com qual nome?',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Nome da anotação'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',  
          cssClass: 'secondary',  
        }, {
          text: 'Confirmar',
          handler: async () => {
            await alert.present();
            let resultado = await alert.onDidDismiss();
            this.nomeAnotacao = resultado.data.values.name;
            this.salvarValor(valor, this.nomeAnotacao);
          }
        }
      ]
    });
    await alert.present();
    
    
  }

}