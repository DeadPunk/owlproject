import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AudioComponent } from './audio.component';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { Media } from '@ionic-native/media/ngx';

@NgModule({
  declarations: [AudioComponent],
  imports: [
    CommonModule,
    IonicModule, 
    FormsModule,
    RouterModule.forChild([{ path: '', component: AudioComponent }])
  ],
  providers: [Media]
})
export class AudioModule { }
