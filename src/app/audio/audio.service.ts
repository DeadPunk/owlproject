import { Injectable } from '@angular/core';
import _ from 'lodash';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})

export class AudioService {

  lista: any;

  constructor(private db: AngularFirestore) {

  }

  getAudioFiles(){
    this.lista = this.db.collection('audio').valueChanges(); 
    console.log('Lista: ',this.lista);
    
    return this.lista;
  }

  getAudioByName(name){
    return _.filter(this.lista, function(e){
      if(e.fileName == name){        
        return e;
      }
    });
  }
}