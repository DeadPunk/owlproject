import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.scss'],
})
export class ManagerComponent implements OnInit {

  name: any;
  played:boolean = false;

  constructor(public route: ActivatedRoute,
    private nativeAudio: NativeAudio
  ) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      (queryParams: any) => {
        this.name = queryParams['name'];
      }
    );
    this.nativeAudio.preloadSimple('file', 'audioFiles/'+this.name).then((resp) => {
    }).catch((err) => {
      console.log(err);
    });
    this.playAudio();
  }

  stopAudio() {
    this.nativeAudio.stop('file').then((resp) => {
    }).catch((err) => {
      console.log(err);
    });
  }

  playAudio() {
    this.played = true;
    this.nativeAudio.play('file').then((resp) => { }).catch((err) => {
      console.log(err);
    });;
  }

  clickButton(){
  if (this.played) {
      this.played = false;
      this.stopAudio();
    }
    else{
      this.played = true;
      this.playAudio();
    }

  }

  back(){
    this.nativeAudio.unload('link').then((resp) => { }).catch((err) => {
      console.log(err);
    });;;
  }
}
