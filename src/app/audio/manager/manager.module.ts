import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ManagerComponent } from './manager.component';
import { NativeAudio } from '@ionic-native/native-audio/ngx';


const routes: Routes = [
  {
    path: '',
    component: ManagerComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ManagerComponent],
  providers: [NativeAudio]
})
export class ManagerModule { }
