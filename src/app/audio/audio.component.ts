import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AudioService } from './audio.service';
import { Router } from '@angular/router';
import { Media, MediaObject } from '@ionic-native/media/ngx';

@Component({
  selector: 'app-audio',
  templateUrl: './audio.component.html',
  styleUrls: ['./audio.component.scss'],
})
export class AudioComponent implements OnInit {

  audioFile: any;
  audio: MediaObject;
  recording: boolean = false;
  texto:string;

  constructor(private audioService: AudioService, 
    public navCtrl: NavController, 
    private router: Router,
    private media: Media
    ) {  }

  ngOnInit() {
    
    this.getAudios()
  }   

  getAudios(){
    this.audioService.getAudioFiles().subscribe((files) => {
      this.audioFile = files;
      // console.log('Audio: ', this.audioFile);
      
    }, (err) => {
      console.log("erro ao recuperar a lista")
      console.log(err);
    });
  }

  openManager(object){
    this.router.navigate(['/manager'], { queryParams: { name: object.fileName } });
  }

  openDetails(object){
    this.router.navigate(['/detalhes'], { queryParams: { name: object.fileName, component: 'audio' } });
  }

  record(){
    if (!this.recording) {
      const fileName = 'record-' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.mp3';
      console.log(fileName);
      
      // this.file.createFile(this.file.tempDirectory)
        //       let file = this.media.create(this.file.tempDirectory.replace(/^file:\/\//, '') + this.filePath);
        //     });)
      this.audio = this.media.create('audioFiles/'+fileName);
      this.audio.startRecord();
      this.texto = 'Clique novamente para parar a gravação!'
      this.recording = true;
    } else {
      this.texto = 'Audio gravado!'
      
      this.audio.stopRecord();
      this.recording = false;
    }
  }
  // startRecord() {

  //     console.log(this.file.);      

  //     this.fileName = 'record' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.3gp';

  //     this.file.createFile(this.file.tempDirectory, this.filePath, true).then(() => {
  //       let file = this.media.create(this.file.tempDirectory.replace(/^file:\/\//, '') + this.filePath);
  //     });

  //     if (this.file.documentsDirectory !== null || this.file.documentsDirectory !== undefined) {
  //       alert(this.file.documentsDirectory);
  //       this.audio = this.media.create(this.filePath);
  //     }
    

  //   this.audio.startRecord();
  //   this.recording = true;
  // }
}
