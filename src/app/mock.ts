import { AudioModel, AnotacaoModel } from './model'

export const AUDIO: AudioModel[] = [
    {fileName: 'desenvolvimento-web.mp4', size: 200, class: 'Desenvolvimento web', date: '02/03/2019'},
    {fileName: 'so-aula-1.mp4', size: 1, class: 'Sistemas Operacionais', date: '20/03/2019'}, 
    {fileName: 'so-aula-2.mp4', size: 300, class: 'Sistemas Operacionais', date: '14/03/2019'},
    {fileName: 'modelagem-1.mp4', size: 10, class: 'Modelagem de Banco de dados', date: '16/03/2019'}
]

export const ANOTACAO: AnotacaoModel[] = [
    {nome: 'web.txt', conteudo: 'teste1'},
    {nome: 'web2.txt', conteudo: 'teste2'},
    {nome: 'web3.txt', conteudo: 'teste3'},
    {nome: 'web4.txt', conteudo: 'teste4'},
];
