// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBTPf9cAeKzwWyBycgmDEXRtoxPmmljlo4",
    authDomain: "owl-project-ceub.firebaseapp.com",
    databaseURL: "https://owl-project-ceub.firebaseio.com",
    projectId: "owl-project-ceub",
    storageBucket: "owl-project-ceub.appspot.com",
    messagingSenderId: "274004037791",
    appId: "1:274004037791:web:d6d6c1c1f461f313"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
